# This security group is created for Bastion host.
# The hosts in private subnet could be accessible through ssh only from Bastion host.

resource "aws_security_group" "bastion_sg" {
  name        = "bastion_sg"
  description = "SG rules for bastion hosts"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["185.223.127.179/32"] # Please change this to your trusted public ip.
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.cidr_web, var.cidr_db]

  }

  tags = {
    Name = "bastion_sg"
  }
}



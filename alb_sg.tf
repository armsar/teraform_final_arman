## Creating security group for Application load balancer.

resource "aws_security_group" "alb_sg" {
  name        = "alb_sg"
  description = "SG rules for ALB"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    description = "http"
    from_port        = 443
    to_port          = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "http"
    from_port        = 0
    to_port          = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb_sg"
  }
}

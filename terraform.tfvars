# Defining values for the variables.

aws_region   = "us-east-1"
cidr_public1 = "10.0.0.0/28"
cidr_web     = "10.0.0.32/28"
cidr_public2 = "10.0.0.16/28"
cidr_db     = "10.0.0.48/28"
cidr_vpc = "10.0.0.0/26"
bastion_ami  = "ami-0dca28296e3990467"
#access_key = ""
#secret_key = ""
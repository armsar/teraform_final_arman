# Creating launch template to be used in web autoscaling group.

resource "aws_launch_template" "webserver_launch_temp" {
  name                                 = "webserver_launch_temp"
  ebs_optimized                        = true
  vpc_security_group_ids               = [aws_security_group.web_sg.id]
  instance_initiated_shutdown_behavior = "terminate"
  disable_api_termination              = false
  image_id                             = "ami-0dca28296e3990467"
  instance_type                        = "t2.nano"

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "web_asg"
    }
  }
}


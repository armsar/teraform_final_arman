# Declaring variables.

variable "cidr_public2" {
  type        = string
  description = "Declaring variable for the cidr of the public2 subnet"
}

variable "cidr_public1" {
  type        = string
  description = "Declaring variable for the cidr of the public1 subnet"
}


variable "cidr_web" {
  type        = string
  description = "Declaring variable for the cidr of the web private subnet"
}

variable "cidr_db" {
  type        = string
  description = "Declaring variable for the cidr of the db private subnet"
}
variable "bastion_ami" {
  type        = string
  description = "Declaring variable for the cidr of the web bastion_ami"
}

variable "cidr_vpc" {
  type        = string
  description = "Declaring variable for the cidr of the web vpc"
}

variable "access_key" {
  type        = string
  description = "Declaring variable for the cidr of the web access_key"
}

variable "secret_key" {
  type        = string
  description = "Declaring variable for the cidr of the web secret_key"
}
